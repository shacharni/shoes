<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
        'name'=>'a',
        'email'=>'a@a.com',
        'password'=>Hash::make('12345678'),
        'role'=>'manager',
    ],
    [
        'name'=>'d',
            'email'=>'d@d.com',
            'password'=>Hash::make('12345678'),
            'role'=>'customer',
        ],
        [
        'name'=>'e',
        'email'=>'e@e.com',
        'password'=>Hash::make('12345678'),
        'role'=>'customer',
    ],
    [
    'name'=>'f',
    'email'=>'f@f.com',
    'password'=>Hash::make('12345678'),
    'role'=>'customer',
],
    
]);

    }
}
