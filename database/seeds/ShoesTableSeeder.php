<?php

use Illuminate\Database\Seeder;

class ShoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shoes')->insert([
            [
        'type'=>'boot',
        'units_in_Stock'=>'10',
        
    ],
    [
        'type'=>'High heels shoes',
        'units_in_Stock'=>'10',
        ],
        [
        'type'=>'Flat shoes',
         'units_in_Stock'=>'10',
    ],
    
]);
    }
}
